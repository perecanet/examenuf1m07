<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductSeeder::class);
    }
}

class ProductSeeder extends Seeder
{
    public function run(){
        DB::table('products')->insert([
            'name'=> 'Lego',
            'image'=> 'lego1.jpeg',
            'price'=> 23,
            'description'=> 'Pack de peces lego 1',
            'puntuation'=> 4,
            'category'=> 1
        ]);

        DB::table('products')->insert([
            'name'=> 'Playmobil',
            'image'=> 'lego2.jpeg',
            'price'=> 15,
            'description'=> 'Pack de playmobil',
            'puntuation'=> 2,
            'category'=> 2
        ]);

        DB::table('products')->insert([
            'name'=> 'Barbie',
            'image'=> 'lego3.jpeg',
            'price'=> 10,
            'description'=> 'Muñeca Barbie',
            'puntuation'=> 5,
            'category'=> 3
        ]);

        DB::table('products')->insert([
            'name'=> 'Lego 2',
            'image'=> 'lego4.jpeg',
            'price'=> 25,
            'description'=> 'Pack de peces lego 2',
            'puntuation'=> 4,
            'category'=> 1
        ]);
    }
}