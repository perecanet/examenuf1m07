<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function scopeStars($query, $nStars) {
        $query1 = $query->where('puntuation','=',$nStars)->get();
        return $query1;
    }
    public function scopeCategory($query, $cat) {
        $query1 = $query->where('category','=',$cat)->get();
        return $query1;
    }
    public function scopePrice($query) {
        $query1 = $query->where('name','LIKE','%')->orderBy('price', 'asc')->get();
        return $query1;
    }
    public function scopeAll($query){
        $query1 = $query->where('name','LIKE','%')->get();
        return $query1;
    }
}
