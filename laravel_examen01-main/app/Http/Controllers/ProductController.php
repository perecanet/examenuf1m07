<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{    
   
    private $products;
    private $_filters;

    public function __construct()
    {
        /**
         * Filters (name=>value) format to show in the view
         * Write the content of the stars
         */
        $this->_filters=(object)array(
            'category'=>array('Category 1'=>'cat1','Category 2'=>'cat3','Category 3'=>'cat3'),
            'stars'=>array('1 estrella'=> 1, '2 estrellas'=> 2)
        );
        $this->products = new Product();
    }
    /**
     * Method to list all the products
     */
    public function all()
    {
        return view('examenViews/products')->with(['products'=>$this->products->all()]);
    }

    /**
     * Method to list the products filtered by category
     */
    public function category(Request $request)
    {   
        return view('examenViews/products')->with(['products'=>$this->products->category($request->input('category'))]);
    }

    /**
     * Method to list the products filtered by stars
     */
    public function stars(Request $request)
    {
        return view('examenViews/products')->with(['products'=>$this->products->stars($request->input('puntuation'))]);
    }
    /**
     * Method to list the products filtered by price
     */
    public function price()
    {
        return view('examenViews/products')->with(['products'=>$this->products->price()]);
    }
}